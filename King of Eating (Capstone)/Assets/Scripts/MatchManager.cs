﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchManager : MonoBehaviour {

    int state = 0;
    public PlayerManager[] players;
    public int numPlayers = 0;
    public GameObject[] guiClumps;
    public GameObject instructor;

    //Keys Set
    bool wasd = false;
    bool lp = false;
    bool abxy = false;
    bool dpad = false;
    //public GameObject playerObj;

    public bool[] takenColors=new bool[6];

	// Use this for initialization
	void Start () {
		for(int i = 0; i < 6; i++)
        {
            takenColors[i] = false;
        }
	}

    // Update is called once per frame
    void Update () {
		switch (state)
        {
            case 0:
                if (Input.anyKeyDown)
                {
                    if ((Input.GetKeyDown(KeyCode.A)|| Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D)) && !wasd)
                    {
                        players[numPlayers].gameObject.SetActive(true);
                        players[numPlayers].setControls(PlayerManager.ControlTypes.WASD);
                        if (!takenColors[numPlayers])
                        {
                            players[numPlayers].myColor = (PlayerManager.PlayerColor)numPlayers;
                            takenColors[numPlayers] = true;
                        }
                        else
                        {
                            for(int i = 0; i < takenColors.Length; i++)
                            {
                                if (!takenColors[i])
                                {
                                    players[i].myColor = (PlayerManager.PlayerColor)numPlayers;
                                    takenColors[i] = true;
                                    break;
                                }
                            }
                        }
                        players[numPlayers].playerID = numPlayers;
                        guiClumps[numPlayers].SetActive(true);
                       // players[numPlayers].guiClump = guiClumps[numPlayers];
                        numPlayers++;
                        wasd = true;
                    }
                    else if ((Input.GetKeyDown(KeyCode.L) || Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Semicolon) || Input.GetKeyDown(KeyCode.Quote)) && !lp)
                    {
                        players[numPlayers].gameObject.SetActive(true);
                        players[numPlayers].setControls(PlayerManager.ControlTypes.PL);
                        if (!takenColors[numPlayers])
                        {
                            players[numPlayers].myColor = (PlayerManager.PlayerColor)numPlayers;
                            takenColors[numPlayers] = true;
                        }
                        else
                        {
                            for (int i = 0; i < takenColors.Length; i++)
                            {
                                if (!takenColors[i])
                                {
                                    players[i].myColor = (PlayerManager.PlayerColor)numPlayers;
                                    takenColors[i] = true;
                                    break;
                                }
                            }
                        }
                        players[numPlayers].playerID = numPlayers;
                    guiClumps[numPlayers].SetActive(true);
                    //players[numPlayers].guiClump = guiClumps[numPlayers];
                    numPlayers++;
                        lp = true;
                    }

            else if ((Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Joystick1Button2) || Input.GetKeyDown(KeyCode.Joystick1Button3)) && !abxy)
            {
                players[numPlayers].gameObject.SetActive(true);
                players[numPlayers].setControls(PlayerManager.ControlTypes.ABXY);
                        if (!takenColors[numPlayers])
                        {
                            players[numPlayers].myColor = (PlayerManager.PlayerColor)numPlayers;
                            takenColors[numPlayers] = true;
                        }
                        else
                        {
                            for (int i = 0; i < takenColors.Length; i++)
                            {
                                if (!takenColors[i])
                                {
                                    players[i].myColor = (PlayerManager.PlayerColor)numPlayers;
                                    takenColors[i] = true;
                                    break;
                                }
                            }
                        }
                        players[numPlayers].playerID = numPlayers;
                guiClumps[numPlayers].SetActive(true);
                //players[numPlayers].guiClump = guiClumps[numPlayers];
                numPlayers++;
                abxy = true;
            }
            //else if ((Input.GetKeyDown(KeyCode.Joystick1Button11) || Input.GetKeyDown(KeyCode.Joystick1Button12) || Input.GetKeyDown(KeyCode.Joystick1Button14) || Input.GetKeyDown(KeyCode.Joystick1Button13)) && !dpad)
            //{
            //    players[numPlayers].gameObject.SetActive(true);
            //    players[numPlayers].setControls(PlayerManager.ControlTypes.DPAD);
            //    players[numPlayers].state = PlayerManager.PlayerState.initControls;
            //    players[numPlayers].playerID = numPlayers;
            //    guiClumps[numPlayers].SetActive(true);
            //    //players[numPlayers].guiClump = guiClumps[numPlayers];
            //    numPlayers++;
            //    dpad = true;
            //}
        }

        if (numPlayers >= 2)
            {
                bool begin = true;
                for (int i = 0; i < numPlayers; i++)
                {
                    if (!players[i].ready)
                    {
                        begin = false;
                    }
                }
                if (begin)
                {
                    state = 1;
                        this.GetComponent<AudioSource>().Play();
                        this.GetComponent<SpriteRenderer>().sprite = null;
                    Debug.Log("game started");
                    for (int i = 0; i < numPlayers; i++)
                    {
                        players[i].wait();
                    }
                }
            }
                break;
            case 1:


                int game=Random.Range(0, 2);
                instructor.GetComponent<Animator>().SetInteger("CurrentInstruction", game);
                for(int i = 0; i < numPlayers; i++)
                {
                    players[i].state = PlayerManager.PlayerState.playGame;
                    players[i].currentMG = game;
                }
                state = 2;
                break;
            case 2:
                for(int i = 0; i < numPlayers; i++)
                {
                    if (players[i].mgProgress>=players[i].mgGoal)
                    {
                        players[i].points++;
                        state = 1;
                        for (int j = 0; j < numPlayers; j++)
                        {
                            players[j].gameComplete();
                        }
                        break;
                    }
                }
                break;
        }
	}

   /* void addPlayer()
    {
        //Instantiate(playerObj, new Vector3(0, 0, 0), Quaternion.identity);
    }*/

    public void snagOtherPlayer(int sourceID, int chosenPlayer)
    {
        int i = chosenPlayer;
        if (players[chosenPlayer].playerID == sourceID)
        {
            i++;
        }

        players[i].opponent = players[sourceID];
        players[sourceID].opponent = players[i];

        players[i].state = PlayerManager.PlayerState.playGame;
        players[i].currentMG = 0;
        players[i].mgProgress = 0;
    }

    public PlayerManager.PlayerColor changePlayerColor(bool up, PlayerManager.PlayerColor current)
    {
        PlayerManager.PlayerColor ret;
        ret = current;
        int j;
        if (up)
        {
            for(int i = 0; i < takenColors.Length; i++)
            {
                j = (i + (int)current) % takenColors.Length;
                if (!takenColors[j])
                {
                    ret = (PlayerManager.PlayerColor)j;
                    takenColors[(int)current] = false;
                    takenColors[j] = true;
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i > -takenColors.Length; i--)
            {
                j = (i + (int)current) % takenColors.Length;
                if (j < 0)
                {
                    j = takenColors.Length+j;
                }
                if (!takenColors[j])
                {
                    ret = (PlayerManager.PlayerColor)j;
                    takenColors[(int)current] = false;
                    takenColors[j] = true;
                    break;
                }
            }
        }
        return ret;
    }
}
