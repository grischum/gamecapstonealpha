﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {
    public enum ControlTypes
    {
        WASD,
        PL,
        ABXY,
        DPAD
    }

    public enum PlayerState
    {
        initControls,
        initWait,
        wait,
        chooseGame,
        chooseOpponent,
        playGame,
        victory,
        pregamePause
    }

    public enum PlayerColor
    {
        red,
        yellow,
        blue,
        green,
        purple,
        gray
    }


    public PlayerManager(int pID)
    {
        playerID = pID;
    }

    public MatchManager matchControl;
    //Canvas myCanvas;
    public GameObject guiClump;
    //BIG ASS BRUTE FORCE SOLUTION FUCK THIS I HATE IT EVERY TIME
    public SpriteRenderer Rules;
    public SpriteRenderer Action;
    public SpriteRenderer IconU;
    public SpriteRenderer IconD;
    public SpriteRenderer IconL;
    public SpriteRenderer IconR;
    public SpriteRenderer DigitA;
    public SpriteRenderer DigitB;
    public SpriteRenderer ArrowRenderer;

    public SpriteRenderer bg;
    ///////////////////////////////
    public Sprite readyRules;
    public Sprite unreadyRules;
    public Sprite circleRules;
    public Sprite mashRules;
    public Sprite smashRules;
    public Sprite versusRules;
    public Sprite chooseRules;

    public Sprite arrowUp;
    public Sprite arrowDown;
    public Sprite arrowLeft;
    public Sprite arrowRight;
    public Sprite actionSandwich;
    public Sprite actionDirection;
    public Sprite iconHead;
    public Sprite iconCircle;
    public Sprite iconVersus;
    public Sprite iconMash;
    public Sprite iconSmash;

    public Sprite number0; public Sprite number1; public Sprite number2; public Sprite number3; public Sprite number4; public Sprite number5; public Sprite number6; public Sprite number7; public Sprite number8; public Sprite number9;

    public Sprite vicrory;
    public Sprite complete;

    public Sprite press;

    public Sprite chomp1;
    public Sprite chomp2;
    public Sprite chomp3;
    public Sprite idle;
    public int chompState = 0;
    ///////////////////////////////

    public int playerID;
    public PlayerColor myColor;

    public float mgProgress = 0;
    public float mgGoal = 100f;
    float waitPlus = 1.5f;
    float waitDefault = 1.5f;
    int next = 0;

    public PlayerState state = PlayerState.initControls;
    public int points;
    public int ptw = 10;
    bool attack = false;
    public bool beaten = false;
    public bool ready=false;
    public PlayerManager opponent;

    KeyCode up;
    KeyCode down;
    KeyCode left;
    KeyCode right;

    int choiceL;
    int choiceU;
    int choiceR;
    public int[] seed;
    int seedCursor = 0;
    public int currentMG;

    AudioSource myAudio;

    public bool IWon;

    // Use this for initialization
    void Awake () {
        matchControl = FindObjectOfType<MatchManager>();
        myAudio = this.GetComponent<AudioSource>();
        IWon = false;
        //myCanvas = GetComponentInParent<Canvas>();
	}
	
	// Update is called once per frame
	void Update () {
        switch (myColor)
        {
            case PlayerColor.red:
                this.GetComponent<SpriteRenderer>().color = Color.red;
                bg.GetComponent<SpriteRenderer>().color = Color.red;
                break;
            case PlayerColor.blue:
                this.GetComponent<SpriteRenderer>().color = Color.blue;
                bg.GetComponent<SpriteRenderer>().color = Color.blue;
                break;
            case PlayerColor.yellow:
                this.GetComponent<SpriteRenderer>().color = Color.yellow;
                bg.GetComponent<SpriteRenderer>().color = Color.yellow;
                break;
            case PlayerColor.green:
                this.GetComponent<SpriteRenderer>().color = Color.green;
                bg.GetComponent<SpriteRenderer>().color = Color.green;
                break;
            case PlayerColor.purple:
                this.GetComponent<SpriteRenderer>().color = new Color(200,50,200);
                bg.GetComponent<SpriteRenderer>().color = new Color(200, 50, 200);
                break;
            case PlayerColor.gray:
                this.GetComponent<SpriteRenderer>().color = Color.gray;
                bg.GetComponent<SpriteRenderer>().color = Color.gray;
                break;
        }

        switch (state)
        {
            //--------------Phase 1 - Get Players--------------//
            //case 0: //Waiting to join game
              /* if (Input.anyKeyDown)
                {
                    if /*&& manager.wasd=true*(Input.GetKeyDown("a") || Input.GetKeyDown("w") || Input.GetKeyDown("s") || Input.GetKeyDown("d"))
                    {
                        up = KeyCode.W;
                        down = KeyCode.S;
                        left = KeyCode.A;
                        right = KeyCode.D;
                        state = 1;
                        ready = false;
                        //Tell manager that player has entered
                    }
                    else if /*&& manager.plsq=true*(Input.GetKeyDown("l") || Input.GetKeyDown("p") || Input.GetKeyDown(KeyCode.Semicolon) || Input.GetKeyDown(KeyCode.Quote))
                    {
                        up = KeyCode.P;
                        down = KeyCode.Semicolon;
                        left = KeyCode.L;
                        right = KeyCode.Quote;
                        state = 1;
                        ready = false;
                        //Tell manager that player has entered
                    }
                }*/
                //break;
            case PlayerState.initControls: //Waiting for game to start
                if (Input.GetKeyDown(right))
                {
                    myColor=matchControl.changePlayerColor(true,myColor);
                }
                if (Input.GetKeyDown(left))
                {
                    myColor = matchControl.changePlayerColor(false, myColor);
                }

                if (!ready)
                {
                    Rules.sprite = unreadyRules;
                    if (Input.GetKeyDown(up))
                    {
                        ready = true;
                        Rules.sprite = readyRules;
                    }
                }
                else
                {
                    if (Input.GetKey(down))
                    {
                        mgProgress += 1.5f;
                    }
                    else
                    {
                        mgProgress = 0;
                    }

                    if (mgProgress > mgGoal)
                    {
                        ready = false;
                        Debug.Log("progress caught up to goal");
                        mgProgress = 0;
                    }

                }
                break;

            //--------Phase 2 - Play Game---------//

            case PlayerState.wait: //Standard waiting
                //mgProgress += Time.deltaTime*20;
                //if(mgProgress >= mgGoal)
                //{
                //    state = PlayerState.chooseGame;
                //    waitPlus = waitDefault;
                //    mgProgress = 0;
                //}
             break;
                //Before first wait is over, instanciate seed[]

            //case PlayerState.chooseGame: //Choose game
            //    //int x = -1;
            //    //if (Input.GetKeyDown(left))
            //    //{
            //    //    x = seedCursor;
            //    //}else if (Input.GetKeyDown(up))
            //    //{
            //    //    x = seedCursor + 1;
            //    //}else if (Input.GetKeyDown(right))
            //    //{
            //    //    x = seedCursor + 2;
            //    //}else if (Input.GetKeyDown(down) && attack)
            //    //{
            //    //    state = 4;
            //    //    break;
            //    //}

            //    //if (x != -1)
            //    //{
            //    //    state = 5;
            //    //    currentMG = seed[x];
            //    //}
            //    IconU.sprite = iconCircle;
            //    IconL.sprite = iconMash;
            //    IconR.sprite = iconSmash;
            //    Action.sprite = actionDirection;
            //    Rules.sprite = chooseRules;
            //    if (attack)
            //    {
            //        IconD.sprite = iconVersus;
            //    }

            //    if (Input.GetKeyDown(up))
            //        {
            //            currentMG=1;
            //        mgProgress = 0;
            //        state = PlayerState.playGame;
            //        GetComponent<Animator>().SetTrigger("Trigger");
            //        ArrowRenderer.GetComponent<Animator>().SetInteger("AnimState", 3);
            //        Rules.sprite = circleRules;
            //    }
            //    else if (Input.GetKeyDown(left))
            //     {
            //        currentMG = 2;
            //        mgProgress = 0;
            //        state = PlayerState.playGame;
            //        GetComponent<Animator>().SetTrigger("Trigger");
            //        ArrowRenderer.GetComponent<Animator>().SetInteger("AnimState", 2);
            //        Rules.sprite = mashRules;
            //    }
            //    else if (Input.GetKeyDown(right))
            //        {
            //        currentMG = 3;
            //        mgProgress = 0;
            //        state = PlayerState.playGame;
            //        GetComponent<Animator>().SetTrigger("Trigger");
            //        ArrowRenderer.GetComponent<Animator>().SetInteger("AnimState", 1);
            //        Rules.sprite = smashRules;
            //    }
            //    else if (Input.GetKeyDown(down) && attack)
            //        {
            //            state = PlayerState.chooseOpponent;
            //        IconU.sprite = iconHead; IconD.sprite = iconHead; IconL.sprite = iconHead; IconR.sprite = iconHead;
            //        mgProgress = 0;
            //        Rules.sprite = versusRules;
            //        break;
            //        }
                
            //    break;

            //case PlayerState.chooseOpponent: //Choose opponent
            //                                 //Ask manager for options, get opponent, then set this and oponent state to 5 and currentMG to 0
            //    //IconU.sprite = iconHead; IconD.sprite = iconHead; IconL.sprite = iconHead; IconR.sprite = iconHead;

            //    if (playerID == 0)
            //    {
            //        IconL.color=Color.yellow;
            //    }
            //    else
            //    {
            //        IconL.color = Color.red;
            //    }

            //    if (playerID == 1)
            //    {
            //        IconU.color = Color.blue;
            //    }
            //    else
            //    {
            //        IconU.color = Color.yellow;
            //    }
            //    if (playerID == 2)
            //    {
            //        IconR.color = Color.green;
            //    }
            //    else
            //    {
            //        IconR.color = Color.blue;
            //    }

            //    switch (matchControl.numPlayers)
            //    {
            //        case 2:
            //            if (Input.GetKeyDown(up))
            //            {
            //                matchControl.snagOtherPlayer(playerID,0);
            //                attack = false;
            //                currentMG = 0;
            //                state = PlayerState.playGame;
            //                GetComponent<Animator>().SetTrigger("Trigger");
            //                IconL.color = Color.white; IconR.color = Color.white; IconU.color = Color.white; IconD.color = Color.white;
            //            }
            //            break;
            //        case 3:
            //            break;
            //        case 4:
            //            break;
            //    }
                
            //    break;

            case PlayerState.playGame: //Play the game
                IconL.sprite = null; IconR.sprite = null; IconU.sprite = null; IconD.sprite = null;
                IconL.color = Color.white; IconR.color = Color.white; IconU.color = Color.white; IconD.color = Color.white;
                Action.sprite = actionSandwich;
                //Action.GetComponentInParent<Transform>().localScale.Set((.1973f / (mgGoal - mgProgress)), .1973f, 0);
                //switch (chompState)
                //{
                //    case 0:
                //        this.GetComponent<SpriteRenderer>().sprite = chomp2;
                //        chompState++;
                //        break;
                //    case 1:
                //        this.GetComponent<SpriteRenderer>().sprite = chomp3;
                //        chompState++;
                //        break;
                //    case 2:
                //        this.GetComponent<SpriteRenderer>().sprite = chomp1;
                //        chompState=0;
                //        break;
                //}


                switch (currentMG)
                {
                    case 0:
                        smashGame();
                        break;
                    case 1:
                        circleGame();
                        break;
                    case 2:
                        mashGame();
                        break;
                    case 3:
                        smashGame();
                        break;
                }
                break;

            case PlayerState.victory:
                break;
        }
	}

    //----------------Useful Commands and Games--------------//

    public void setControls(ControlTypes type)
    {
        switch (type){
            case ControlTypes.WASD:
                up = KeyCode.W;
                down = KeyCode.S;
                left = KeyCode.A;
                right = KeyCode.D;
                state = PlayerState.initControls;
                ready = false;
                break;
            case ControlTypes.PL:
                up = KeyCode.P;
                down = KeyCode.Semicolon;
                left = KeyCode.L;
                right = KeyCode.Quote;
                state = PlayerState.initControls;
                ready = false;
                break;

            case ControlTypes.ABXY:
                up = KeyCode.Joystick1Button3;
                down = KeyCode.Joystick1Button0;
                left = KeyCode.Joystick1Button2;
                right = KeyCode.Joystick1Button1;
                state = PlayerState.initControls;
                ready = false;
                break;

            case ControlTypes.DPAD:
                up = KeyCode.Joystick1Button11;
                down = KeyCode.Joystick1Button12;
                left = KeyCode.Joystick1Button13;
                right = KeyCode.Joystick1Button14;
                state = PlayerState.initControls;
                ready = false;
                break;
        }
    }

    public void wait()
    {
        mgProgress = 0;
        
        state = PlayerState.wait;
        IWon = false;
    }
    public void wait(float time)
    {
        mgProgress = 0;
        state = PlayerState.wait;
        waitPlus = mgGoal / time;
    }

    public void gameComplete()
    {
        Debug.Log("Game Completed");
        GetComponent<Animator>().SetTrigger("Trigger");
        ArrowRenderer.GetComponent<Animator>().SetInteger("AnimState",0);
        Action.GetComponentInParent<Transform>().localScale.Set(.1973f, .1973f, 0);
        Action.sprite = complete;
        //seedCursor++;
        //points++;
        IWon = true;
        this.GetComponent<SpriteRenderer>().sprite = idle;
        switch (points % 10)
        {
            case 0:
                DigitB.sprite = number0;
                break;
            case 1:
                DigitB.sprite = number1;
                break;
            case 2:
                DigitB.sprite = number2;
                break;
            case 3:
                DigitB.sprite = number3;
                break;
            case 4:
                DigitB.sprite = number4;
                break;
            case 5:
                DigitB.sprite = number5;
                break;
            case 6:
                DigitB.sprite = number6;
                break;
            case 7:
                DigitB.sprite = number7;
                break;
            case 8:
                DigitB.sprite = number8;
                break;
            case 9:
                DigitB.sprite = number9;
                break;
        }
        if (points >= 10)
        {
            DigitA.sprite = number1;
        }
        mgProgress = 0;
        if (points % 5 == 0)
        {
            attack = true;
        }
        if (points == ptw)
        {
            state = PlayerState.victory;
            SceneManager.LoadScene("Menu");
        }
        else
        {
            wait();
        }
    }

    void snatch(PlayerManager opponent)
    {
        opponent.state = PlayerState.playGame;
        opponent.currentMG = 0;
    }

    void attackGame()
    {
        if (Input.GetKeyDown(down))
        {
            mgProgress += 1;
        }

        if (!beaten)
        {
            //Run game as normal
            if (mgProgress >= mgGoal)
            {
                //gameComplete();
            }
        }
        else
        {
            //kick player back to waiting
            beaten = false;
            wait();
        }
    }

    void circleGame()
    {
        switch (next)
        {
            case 0:
                ArrowRenderer.sprite = arrowUp;
                if (Input.GetKeyDown(up))
                {
                    GetComponent<AudioSource>().Play();
                    mgProgress += 4;
                    next++;
                    
                }
                break;
            case 1:
                ArrowRenderer.sprite = arrowRight;
                if (Input.GetKeyDown(right))
                {
                    GetComponent<AudioSource>().Play();
                    mgProgress += 4;
                    next++;
                    
                }
                break;
            case 2:
                ArrowRenderer.sprite = arrowDown;
                if (Input.GetKeyDown(down))
                {
                    GetComponent<AudioSource>().Play();
                    mgProgress += 4;
                    next++;
                    
                }
                break;
            case 3:
                ArrowRenderer.sprite = arrowLeft;
                if (Input.GetKeyDown(left))
                {
                    GetComponent<AudioSource>().Play();
                    mgProgress += 4;
                    next=0;
                    
                }
                break;
        }
        ArrowRenderer.GetComponent<Animator>().SetInteger("CircleState", next);
        if (mgProgress >= mgGoal)
        {
            //gameComplete();
            next = 0;
        }
    }

    void mashGame()
    {
        if(Input.GetKeyDown(up)|| Input.GetKeyDown(down)|| Input.GetKeyDown(left)|| Input.GetKeyDown(right))
        {
            mgProgress += 2;
            GetComponent<AudioSource>().Play();
        }
        if (mgProgress >= mgGoal)
        {
            //gameComplete();
        }
    }

    void smashGame()
    {
        ArrowRenderer.sprite = arrowDown;
        //ArrowRenderer.GetComponentInParent<Transform>().localScale.Set(-.1973f, .1973f, 0);
        if (Input.GetKeyDown(down))
        {
            mgProgress += 5;
            GetComponent<AudioSource>().Play();
        }
        if (mgProgress >= mgGoal)
        {
            gameComplete();
        }
    }



}
