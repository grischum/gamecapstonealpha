﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {
    int state = 0;
    public Vector3 positionA;
    public Vector3 positionB;
    Transform myTransform;
    AudioSource myAudio;
    
	// Use this for initialization
	void Start () {
        myTransform = this.GetComponent<Transform>();
        myAudio = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.W)|| Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.JoystickButton11)||Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.Semicolon) || Input.GetKeyDown(KeyCode.Joystick1Button12))
        {
            myAudio.Play();
            state = (state + 1) % 2;
            switch (state)
            {
                case 0:
                    myTransform.position = positionA;
                    break;
                case 1:
                    myTransform.position = positionB;
                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Quote) || Input.GetKeyDown(KeyCode.JoystickButton14) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.L) || Input.GetKeyDown(KeyCode.Joystick1Button13))
        {
            switch (state){
                case 0:
                    SceneManager.LoadScene("PlayerManagerTest");
                    //Application.LoadLevel("PlayerManagerTest");
                    break;
                case 1:
                    Application.Quit();
                    break;
            }
        }
    }
}
