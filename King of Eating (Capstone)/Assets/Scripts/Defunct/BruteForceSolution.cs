﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BruteForceSolution : MonoBehaviour
{
    public int gameState = 0;
    public int timer = 0;
    public int timerMax = 100;
    public int timerTick = 3;
    public int mash = 10;
    public int smashLeft = 7;
    public int smashDown = 7;
    public int smashRight = 7;
    public int smashUp = 7;
    public int circle = 0;
    public int circleInc = 10;
    bool pointMash = true;
    bool pointSmash = true;
    bool pointCircle = true;

    bool combatAvailable;

    public GameObject opponent;
    minigameDriver opponentDriver;

    //"UI" Values
    public Sprite number1;
    public Sprite number2;
    public Sprite number3;
    GameObject pointDisplay;

    public Sprite letterLeft;
    public Sprite letterDown;
    public Sprite letterRight;
    public Sprite letterUp;
    GameObject promptDisplay;

    public Sprite wordCircle;
    public Sprite wordMash;
    public Sprite wordSmash;
    public Sprite wordReady;
    public Sprite wordWin;
    GameObject playingDisplay;

    public Sprite wordChoose;

    public int points = 0;
    GameObject ph;
    float scale;

    private void Awake()
    {
        ph = GameObject.Find("PlaceholderC");
        Debug.Log(ph.transform.localScale.x);

        pointDisplay = GameObject.Find("P2Points");
        promptDisplay = GameObject.Find("P2Prompt");
        playingDisplay = GameObject.Find("P2Playing");

        opponentDriver = opponent.GetComponent<minigameDriver>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //start and end play of mash
        if (Input.GetKeyDown("left")&&pointMash)
        {
            if (gameState == 0)
            {
                timer = 0;
                gameState = 1;
                Debug.Log("Game Started!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordMash;
            }
            /*else if (gameState == 1)
            {
                gameState = 0;
                Debug.Log("Game Ended!");
            }*/
        }
        if (Input.GetKeyDown("right")&&pointSmash)
        {
            if (gameState == 0)
            {
                timer = 0;
                gameState = 2;
                Debug.Log("Game Started!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordSmash;
            }
           /*else if (gameState == 2)
            {
                gameState = 0;
                Debug.Log("Game Ended!");
            }*/
        }
        if (Input.GetKeyDown("up")&&pointCircle)
        {
            if (gameState == 0)
            {
                timer = 0;
                gameState = 3;
                Debug.Log("Game Started!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordCircle;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterUp;
            }
            /*else if (gameState == 3)
            {
                gameState = 0;
                Debug.Log("Game Ended!");
            }*/
        }

        if (Input.GetKeyDown("down") && combatAvailable)
        {
            if (gameState == 0)
            {
                timer = 0;
                gameState = 4;
                opponentDriver.timer = 0;
                opponentDriver.gameState = 4;
            }
        }

        if (gameState == 5)
        {
            if (timer > 0)
            {
                timer--;
            }
            if (timer <= 0)
            {
                timer = 0;
                gameState = 0;
            }
        }
        else

        if (gameState != 0)
        {
            //make the timer tick
            if (timer < 0)
            {

                timer = 0;
            }
            else if (timer > 0)
            {
                timer -= timerTick;
            }

            //finish the minigame
            if (timer >= timerMax)
            {
                switch (gameState)
                {
                    case 1:
                        /*if (pointMash) { */
                        points++; pointMash = false; /*} else { pointMash = true; }*/
                        if (opponentDriver.points > points) { combatAvailable = true; }
                        break;
                    case 2:
                        /*if (pointSmash) {*/
                        points++; pointSmash = false; /*} else { pointSmash = true; }*/
                        if (opponentDriver.points > points) { combatAvailable = true; }
                        break;
                    case 3:
                        /*if (pointCircle) {*/
                        points++; pointCircle = false; /*} else { pointCircle = true; }*/
                        if (opponentDriver.points > points) { combatAvailable = true; }
                        break;
                    case 4:
                        points++;
                        opponentDriver.gameState = 5;
                        combatAvailable = false;
                        break;
                }
                gameState = 5;
                Debug.Log("Game Completed!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordReady;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = wordChoose;
                switch (points)
                {
                    case 1:
                        pointDisplay.GetComponent<SpriteRenderer>().sprite = number1;
                        break;
                    case 2:
                        pointDisplay.GetComponent<SpriteRenderer>().sprite = number2;
                        break;
                    case 3:
                        pointDisplay.GetComponent<SpriteRenderer>().sprite = number3;

                        break;
                    case 10:
                        playingDisplay.GetComponent<SpriteRenderer>().sprite = wordWin;
                        break;
                }

            }
        }

        if (gameState == 1)
        {
            promptDisplay.GetComponent<SpriteRenderer>().sprite = letterLeft;
            //accept mash inputs
            if (Input.GetKeyDown("left"))
            {
                timer += mash;
            }

            //Debug.Log("Timer:"+timer);
        }

        if (gameState == 2)
        {
            int i = Random.Range(0,4);
            switch (i){ case 0: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterLeft;break;
                case 1: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterRight; break;
                case 2: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterDown; break;
                case 3: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterUp; break;
            }
            if (Input.GetKeyDown("left"))
            {
                timer += smashLeft;
                smashLeft -= 3;
                if (smashLeft <= 0)
                {
                    smashLeft = 1;
                }
            }
            if (Input.GetKeyDown("down"))
            {
                timer += smashDown;
                smashDown -= 3;
                if (smashDown <= 0)
                {
                    smashDown = 1;
                }
            }
            if (Input.GetKeyDown("right"))
            {
                timer += smashRight;
                smashRight -= 3;
                if (smashRight <= 0)
                {
                    smashRight = 1;
                }
            }
            if (Input.GetKeyDown("up"))
            {
                timer += smashUp;
                smashUp -= 3;
                if (smashUp <= 0)
                {
                    smashUp = 1;
                }
            }

            if (smashLeft < 7)
            {
                smashLeft++;
            }
            if (smashDown < 7)
            {
                smashDown++;
            }
            if (smashRight < 7)
            {
                smashRight++;
            }
            if (smashUp < 7)
            {
                smashUp++;
            }
        }

        if (gameState == 3)
        {
            if (circle == 0 && Input.GetKeyDown("up"))
            {
                timer += circleInc;
                circle++;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterRight;
            }
            if (circle == 1 && Input.GetKeyDown("right"))
            {
                timer += circleInc;
                circle++;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterDown;
            }
            if (circle == 2 && Input.GetKeyDown("down"))
            {
                timer += circleInc;
                circle++;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterLeft;
            }
            if (circle == 3 && Input.GetKeyDown("left"))
            {
                timer += circleInc;
                circle = 0;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterUp;
            }
        }

        if (gameState == 4)
        {
            promptDisplay.GetComponent<SpriteRenderer>().sprite = letterRight;
            //accept mash inputs
            if (Input.GetKeyDown("right"))
            {
                timer += mash;
            }
        }

        scale = (((float)(timerMax - timer) / (float)timerMax) * .7f) + ph.transform.localScale.x;
        ph.transform.localScale -= new Vector3(scale, 0, 0);
        Debug.Log(ph.transform.localScale.x);

    }
}
