﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minigameDriver : MonoBehaviour {
    public int gameState=0;
    public int timer=0;
    public int timerMax=100;
    public int timerTick=3;
    public int mash=10;
    public int smashA = 7;
    public int smashS = 7;
    public int smashD = 7;
    public int smashW = 7;
    public int circle = 0;
    public int circleInc = 10;
    bool pointMash=true;
    bool pointSmash = true;
    bool pointCircle = true;
    bool combatAvailable;
    public GameObject opponent;
    BruteForceSolution opponentDriver;

    //"UI" Values
    public Sprite number1;
    public Sprite number2;
    public Sprite number3;
    GameObject pointDisplay;

    public Sprite letterA;
    public Sprite letterS;
    public Sprite letterD;
    public Sprite letterW;
    GameObject promptDisplay;

    public Sprite wordCircle;
    public Sprite wordMash;
    public Sprite wordSmash;
    public Sprite wordReady;
    public Sprite wordWin;
    GameObject playingDisplay;

    public Sprite wordChoose;

    public int points = 0;
    GameObject ph;
    float scale;

    private void Awake()
    {
        ph = GameObject.Find("PlaceholderA");
        Debug.Log(ph.transform.localScale.x);

        pointDisplay = GameObject.Find("P1Points");
        promptDisplay = GameObject.Find("P1Prompt");
        playingDisplay = GameObject.Find("P1Playing");

        opponentDriver = opponent.GetComponent<BruteForceSolution>();
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        //start and end play of mash
        if (Input.GetKeyDown("a"))
        {
            if (gameState==0&&pointMash)
            {
                timer = 0;
                gameState = 1;
                Debug.Log("Game Started!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordMash;
            }
            /*else if(gameState==1)
            {
                gameState = 0;
                Debug.Log("Game Ended!");
            }*/
        }
        if (Input.GetKeyDown("d"))
        {
            if (gameState == 0&&pointSmash)
            {
                timer = 0;
                gameState = 2;
                Debug.Log("Game Started!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordSmash;
            }
            /*else if (gameState == 2)
            {
                gameState = 0;
                Debug.Log("Game Ended!");
            }*/
        }
        if (Input.GetKeyDown("w")&&pointCircle)
        {
            if (gameState == 0)
            {
                timer = 0;
                gameState = 3;
                Debug.Log("Game Started!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordCircle;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterW;

            }
            /*else if (gameState == 3)
            {
                gameState = 0;
                Debug.Log("Game Ended!");
            }*/
        }
        if (Input.GetKeyDown("s") && combatAvailable)
        {
            if(gameState == 0)
            {
                timer = 0;
                gameState = 4;
                opponentDriver.timer = 0;
                opponentDriver.gameState = 4;
            }
        }

        if(gameState == 5)
        {
            if(timer > 0)
            {
                timer--;
            }
            if (timer <= 0)
            {
                timer = 0;
                gameState = 0;
            }
        }else

        if (gameState != 0)
        {
            //make the timer tick
            if (timer < 0)
            {
                timer = 0;
            }
            else if (timer > 0)
            {
                timer -= timerTick;
            }

            //finish the minigame
            if (timer >= timerMax)
            {
                switch (gameState)
                {
                    case 1:
                        /*if (pointMash) { */points++; pointMash = false; /*} else { pointMash = true; }*/
                        if (opponentDriver.points > points) { combatAvailable = true; }
                        break;
                    case 2:
                        /*if (pointSmash) {*/ points++; pointSmash = false; /*} else { pointSmash = true; }*/
                        if (opponentDriver.points > points) { combatAvailable = true; }
                        break;
                    case 3:
                        /*if (pointCircle) {*/ points++; pointCircle = false; /*} else { pointCircle = true; }*/
                        if (opponentDriver.points > points) { combatAvailable = true; }
                        break;
                    case 4:
                        points++;
                        opponentDriver.gameState = 5;
                        combatAvailable = false;
                        break;
                }
                gameState = 5;
                Debug.Log("Game Completed!");
                playingDisplay.GetComponent<SpriteRenderer>().sprite = wordReady;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = wordChoose;
                switch (points)
                {
                    case 1:
                        pointDisplay.GetComponent<SpriteRenderer>().sprite = number1;
                        break;
                    case 2:
                        pointDisplay.GetComponent<SpriteRenderer>().sprite = number2;
                        break;
                    case 3:
                        pointDisplay.GetComponent<SpriteRenderer>().sprite = number3;
                        
                        break;
                    case 10:
                        playingDisplay.GetComponent<SpriteRenderer>().sprite = wordWin;
                        break;
                }
            }
        }

        if (gameState==1)
        {
            promptDisplay.GetComponent<SpriteRenderer>().sprite = letterA;
            //accept mash inputs
            if (Input.GetKeyDown("a"))
            {
                timer += mash;
            }

            //Debug.Log("Timer:"+timer);
        }

        if (gameState == 2)
        {
            int i = Random.Range(0, 4);
            switch (i)
            {
                case 0: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterA; break;
                case 1: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterD; break;
                case 2: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterS; break;
                case 3: promptDisplay.GetComponent<SpriteRenderer>().sprite = letterW; break;
            }

            if (Input.GetKeyDown("a"))
            {
                timer += smashA;
                smashA -= 3;
                if (smashA <= 0)
                {
                    smashA = 1;
                }
            }
            if (Input.GetKeyDown("s"))
            {
                timer += smashS;
                smashS -= 3;
                if (smashS <= 0)
                {
                    smashS = 1;
                }
            }
            if (Input.GetKeyDown("d"))
            {
                timer += smashD;
                smashD -= 3;
                if (smashD <= 0)
                {
                    smashD = 1;
                }
            }
            if (Input.GetKeyDown("w"))
            {
                timer += smashW;
                smashW -= 3;
                if (smashW <= 0)
                {
                    smashW = 1;
                }
            }

            if (smashA < 7)
            {
                smashA++;
            }
            if (smashS < 7)
            {
                smashS++;
            }
            if (smashD < 7)
            {
                smashD++;
            }
            if (smashW < 7)
            {
                smashW++;
            }
        }

        if (gameState == 3)
        {
            if (circle == 0 &&Input.GetKeyDown("w"))
            {
                timer += circleInc;
                circle++;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterD;
            }
            if (circle == 1 && Input.GetKeyDown("d"))
            {
                timer += circleInc;
                circle++;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterS;
            }
            if (circle == 2 && Input.GetKeyDown("s"))
            {
                timer += circleInc;
                circle++;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterA;
            }
            if (circle == 3 && Input.GetKeyDown("a"))
            {
                timer += circleInc;
                circle=0;
                promptDisplay.GetComponent<SpriteRenderer>().sprite = letterW;
            }
        }

        if(gameState == 4)
        {
            promptDisplay.GetComponent<SpriteRenderer>().sprite = letterA;
            //accept mash inputs
            if (Input.GetKeyDown("a"))
            {
                timer += mash;
            }
        }


        scale = (((float)(timerMax-timer) / (float)timerMax)*.7f)+ph.transform.localScale.x;
        ph.transform.localScale -= new Vector3(scale,0,0);
        Debug.Log(ph.transform.localScale.x);

    }
}
